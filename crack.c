#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "md5.h"

const int PASS_LEN=50;        // Maximum any password will be


// Stucture to hold both a plaintext password and a hash.
struct entry 
{
    char plain[50];
    char hash[33];// TODO: FILL THIS IN
};

int byHash(const void *a, const void *b);
int searchHash(const void *t, const void *elem);
void printFound(struct entry f);


// TODO
// Read in the dictionary file and return an array of entry structs.
// Each entry should contain both the hash and the dictionary
// word.
struct entry *read_dictionary(char *filename, int *size)
{
    FILE *df = fopen(filename, "r");
    if (df == NULL)
    {
        perror("Can't open given file");
        exit(1);
    }
    int arraylength = 10;
    struct entry *info = malloc(arraylength * sizeof(struct entry));
	int entries = 0;
	char line[20];
	while(fgets(line, 20, df) != NULL)
	{
		char *nl = strchr(line, '\n');
        if (nl != NULL)
        {
            *nl = '\0';
        }
		if(entries == arraylength)
		{
			arraylength += 10;
			info = realloc(info, arraylength*sizeof(struct entry));
		}
		//info.plain = line;
		strcpy(info[entries].plain, line);
		char *hash = md5(line, strlen(line));
		strcpy(info[entries].hash, hash);
		entries++;
	}
    fclose(df);
    *size = entries;
    return info;
}


int main(int argc, char *argv[])
{
    int size;
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }
    

    // TODO: Read the dictionary file into an array of entry structures
    struct entry *dict = read_dictionary(argv[2], &size);
    /*
    printf("Before Sort\n");
    for(int i = 0; i < 10; i++)
    {
        printf("%s %s \n", dict[i].plain, dict[i].hash);
    }
    */
    
    
    // TODO: Sort the hashed dictionary using qsort.
    // You will need to provide a comparison function that
    // sorts the array by hash value.
    qsort(dict, size, sizeof(struct entry), byHash);
    /*
    printf("After Sort\n");
    for(int i = 0; i < 10; i++)
    {
        printf("%s %s \n", dict[i].plain, dict[i].hash);
    }
    */

    
    // Partial example of using bsearch, for testing. Delete this line
    // once you get the remainder of the program working.
    // This is the hash for "rockyou"

    // TODO
    // Open the hash file for reading.
    FILE *hf = fopen(argv[1], "r");
    if (hf == NULL)
    {
        perror("Can't open given file");
        exit(1);
    }
    char hline[34];
    int count = 0;
	while(fgets(hline, 34, hf) != NULL)
	{
		char *nl = strchr(hline, '\n');
        if (nl != NULL)
        {
            *nl = '\0';
        }
        struct entry *found = bsearch(hline, dict, size, sizeof(struct entry), searchHash);
        if(found)
        {
            count ++;
            printFound(*found);
        }
        printf("%d\n", count);
	}
        return 0;
    // TODO
    // For each hash, search for it in the dictionary using
    // binary search.
    // If you find it, get the corresponding plaintext dictionary word.
    // Print out both the hash and word.
    // Need only one loop. (Yay!)
}
void printFound(struct entry f)
{
	printf("%s %s\n", f.plain, f.hash);
}

int byHash(const void *a, const void *b)
{
	struct entry *aa = (struct entry *)a;
	struct entry *bb = (struct entry *)b;
	
	return strcmp(aa->hash, bb->hash);
}

int searchHash(const void *t, const void *elem)
{
	char *tt = (char *)t;
	struct entry *eelem = (struct entry *)elem;
	
	return strcmp(tt, eelem->hash);
}
